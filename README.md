
## 技术概述
* 后端采用Spring Boot、Spring Cloud & Alibaba。
* 注册中心、配置中心：Nacos
* 网关路由代理调用：Spring Cloud Gateway (动态网关)
* 声明式服务调用：Spring Cloud OpenFeign
* 阿里流量防卫兵：Sentinel (限流、熔断降级、负载保护)
* 分布式事务框架：TX-LCN（LCN模式）
* 数据库集群： ShardingSphere 5.0 + MySQL 8.0


## 系统模块
~~~
hxds-cloud     
├── hxds-driver-wx              // 司机端小程序 
├── hxds-customer-wx            // 客户端小程序
├── hxds-mis-vue                // MIS系统前端项目 [3000]
├── hxds-                
│       └── bff-customer                  // 客户bff子系统 [8102]
│       └── bff-driver                    // 司机bff子系统 [8101]
│       └── common                        // 通用模块
│       └── gateway                       // 网关子系统 [8080]
│       └── hxds-cst                      // 客户子系统 [8007]
│       └── hxds-dr                       // 司机子系统 [8001]
│       └── yhxds-mis-api                 // MIS子系统 [8010]
│       └── hxds-mps                      // 地图子系统 [8004]
│       └── hxds-nebula                   // 大数据子系统 [8009]
│       └── hxds-odr                      // 订单子系统 [8002]
│       └── hxds-rule                     // 规则子系统 [8006]
│       └── hxds-snm                      // 消息通知子系统 [8003]
│       └── hxds-tm                       // 分布式事务管理节点 [7970] [8070]
│       └── pom.xml                       // 公共依赖
│       └── .txlcn                        // 分布式
~~~

## 架构图
![输入图片说明](images/%E7%B3%BB%E7%BB%9F%E6%9E%B6%E6%9E%84%E5%9B%BE.jpg)

## 微服务BFF鉴权（[SaToken认证与授权](https://sa-token.cc/)）
客户端不是直接访问服务器的公共接口，而是调用BFF层提供的接口，BFF层再调用基础的服务，不同的客户端拥有不同的BFF层，它们定制客户端需要的API接口。
有了BFF层之后，客户端只需要发起一次HTTP请求，BFF层就能调用不同的服务，然后把汇总后的数据返回给客户端，这样就减少了外网的HTTP请求，响应速度也就更快。
![输入图片说明](images/BFF%E9%89%B4%E6%9D%83.png)


## 腾讯云服务
| 序号 | 云服务名称           | 具体实现                         |
|----|-----------------|------------------------------|
| 1  | 对象存储服务（COS）        | 存储司机实名认证的身份证和驾驶证照片           |
| 2  | 人脸识别（AiFace）         | 司机接单前的身份核实，并且具备静态活体检测功能    |
| 3  | 人员库管理（face-lib）     | 云端存储司机注册时候的人脸模型，用于身份比对使用     |
| 4  | 数据万象（ci）             | 用于监控大家录音文本内容，判断是否包含违规内容     |
| 5  | OCR证件识别插件            | 用于OCR识别和扫描身份证和驾驶证的信息         |
| 6  | 微信同声传译插件           | 把文字合成语音，播报订单；把录音转换成文本，用于安全监控 |
| 7  | 路线规划插件              | 用于规划司机下班回家的线路，或者小程序订单显示的路线   |
| 8  | 地图选点插件              | 用于小程序上面地图选点操作                |
| 9  | 腾讯位置服务              | 路线规划、定位导航、里程和时间预估            |


## 在线体验

演示地址：  


## 项目流程图
![输入图片说明](images/%E6%B5%81%E7%A8%8B%E5%9B%BE.jpg)

## 演示图
### 1、司机乘客登录、注册界面
![输入图片说明](images/19b42ea4ce97ae85bed914efd42c06cd.png)
### 2、司机乘客工作台界面
![输入图片说明](images/a3f5e7c370d7e4fbe78241e0bace415d.png)
### 3、司机实名认证界面
![输入图片说明](images/c69fbd4bb2a27c9d007ea11a7ab4bbc9.png)
### 4、工作人员审核司机实名认证界面
### 5、客户代驾选起始点、车辆、下单界面
![输入图片说明](images/88662dec6d60331055efc705ff834e19.png)
### 6、司机开始接单界面
![输入图片说明](images/b64cd58bef9c5eea8dd5bd17f59b7674.png)
### 7、乘客创建订单、司机抢单界面
![输入图片说明](images/6b7e7f9edf79a90541578950e06cc570.png)
![输入图片说明](images/c90024c1735b2ff6d76e5669a353df3a.png)
### 8、司机赶往代驾起点、到达代驾点，乘客确认司机到达
![输入图片说明](images/97ff29bd44341881942d68cf496f6ece.png)
### 9、开始代驾，司乘同显界面
![输入图片说明](images/0ba6552a69155f0ad8b2378dcfde7b10.png)
### 10、到达终点、结束代驾，并且推送账单给客户
![输入图片说明](images/6f32414dd31722411474284439bb8e3d.png)
![输入图片说明](images/dc67a6a7bb2f54030c896b98ea083448.png)
### 11、乘客支付成功、评价订单
![输入图片说明](images/8b522364a38af29202b9158a3b62ddc0.png)

## 技术栈总结
| 序号 | 技术栈        | 具体实现                   |
|----|------------|------------------------|
| 1  | SpringBoot | 创建微服务子系统               |
| 2  | SpringMVC  | Web层框架                 |
| 3  | MyBatis    | 持久层框架                  |
| 4  | Feign      | 远程调用                   |
| 5  | TX-LCN     | 分布式事务                  |
| 6  | RabbitMQ   | 系统消息收发                 |
| 7  | Swagger    | 在线调试Web方法              |
| 8  | QLExpress  | 规则引擎，计算预估费用、取消费用等等     |
| 9  | Quartz     | 定时器，销毁过期未接单订单、定时自动分账等等 |
| 10  | Phoenix     | HBase数据存储                  |
| 11  | Minio     | 私有云存储                 |
| 12  | GEO       | GPS分区定位计算              |
| 13  | SaToken   | 认证与授权框架     |
| 14  | VUE3.0    | 前端框架 |
| 15  | UniAPP    | 移动端框架 |
